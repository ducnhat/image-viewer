/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';

import ImageViewer from '@bcms-demo/new-project';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <ImageViewer style={ProductViewerStyles} />
      <SafeAreaView>
        <ScrollView
          style={styles.scrollView}>
          <View style={styles.body}>
            <View>
              <Text style={{fontSize: 28, marginTop: 10}}>Image Viewer</Text>
              <Text>
                This is an example that show how to create and publish npm
                module
              </Text>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    // backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: Colors.white,
  },
});

const ProductViewerStyles = StyleSheet.create({
  container: {
    // flex: 1,
    marginTop: 32,
    height: 400,
  },
});

export default App;

import React, {Component} from 'react';
import {View, FlatList, Image} from 'react-native';

class ImageViewer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            images: [
                {uri: 'http://i.imgur.com/5nltiUd.jpg'},
                {uri: 'http://i.imgur.com/XP2BE7q.jpg'},
                {uri: 'http://i.imgur.com/6vOahbP.jpg'},
                {uri: 'http://i.imgur.com/kj5VXtG.jpg'}
            ],
        }
    }

    render() {
        console.log(this.props.style.container, 1);
        return (
            <View style={this.props.style.container}>
                <FlatList
                    style={{height: 400}}
                    data={this.state.images}
                    horizontal={true}
                    renderItem={({item}) => {
                        return (
                            <Image source={{uri: item.uri}}
                                   style={{width: 400, height: 400}}/>
                        )
                    }}
                    keyExtractor={item => item.uri}
                />
            </View>
        )
    }
}

export default ImageViewer;


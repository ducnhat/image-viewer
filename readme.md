# How to create and publish npm package

### 1. Create npm project
- Create directory `mkdir new-project`
- Go to created directory `cd new-project`
- Init new npm package `npm init` and then answer the questions
```text
question name (new-project): @bcms-demo/new-project
question version (1.0.0): 
question description: Demo project how to create and publish npm package
question entry point (index.js): 
question repository url: 
question author: 
question license (MIT): 
question private: false
```
- After this step, you will have `package.json` file in your project with content:
```json
{
    "name": "@bcms-demo/new-project",
    "version": "1.0.0",
    "description": "Demo project how to create and publish npm package",
    "main": "index.js",
    "license": "MIT",
    "private": false
}
```

### 2. Coding
- Create `index.js` file in root directory with below content
```javascript
module.exports = require('./src/ImageViewer');
```
- Create `src` directory in root directory
- Go to `src` directory and create `ImageViewer.js` file
- Open `ImageViewer.js` file and add this content
```javascript
 import React, {Component} from 'react';
 import {View, FlatList, Image} from 'react-native';
 
 class ImageViewer extends Component {
     constructor(props) {
         super(props);
         this.state = {
             images: [
                 {uri: 'http://i.imgur.com/5nltiUd.jpg'},
                 {uri: 'http://i.imgur.com/XP2BE7q.jpg'},
                 {uri: 'http://i.imgur.com/6vOahbP.jpg'},
                 {uri: 'http://i.imgur.com/kj5VXtG.jpg'}
             ],
         }
     }
 
     render() {
         console.log(this.props.style.container, 1);
         return (
             <View style={this.props.style.container}>
                 <FlatList
                     style={{height: 400}}
                     data={this.state.images}
                     horizontal={true}
                     renderItem={({item}) => {
                         return (
                             <Image source={{uri: item.uri}}
                                    style={{width: 400, height: 400}}/>
                         )
                     }}
                     keyExtractor={item => item.uri}
                 />
             </View>
         )
     }
 }
 
 export default ImageViewer;
```
### 3. Testing
- Pack your package `npm pack`, after finishing you will have `bcms-demo-new-project-1.0.0.tgz` file in your project
- Create example react native app to test your package `react-native init examples`
- Go to `examples` directory, and install your package `npm i ../bcms-demo-new-project-1.0.0.tgz`
- Open `examples/App.js` file and replace the content by below content
```javascript
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';

import ImageViewer from '@bcms-demo/new-project';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <ImageViewer style={ProductViewerStyles} />
      <SafeAreaView>
        <ScrollView
          style={styles.scrollView}>
          <View style={styles.body}>
            <View>
              <Text style={{fontSize: 28, marginTop: 10}}>Image Viewer</Text>
              <Text>
                This is an example that show how to create and publish npm
                module
              </Text>
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    // backgroundColor: Colors.lighter,
  },
  body: {
    backgroundColor: Colors.white,
  },
});

const ProductViewerStyles = StyleSheet.create({
  container: {
    // flex: 1,
    marginTop: 32,
    height: 400,
  },
});

export default App;
```
- Run example app `cd examples && npm run start && npm run ios`

### 4. Create npm scope
- Go to [https://www.npmjs.com/org/create](https://www.npmjs.com/org/create), login (or create) with your account
- Create new scope named `bcms-demo`

### 5. Publish your package
- Login with your [npm account](https://www.npmjs.com/) `yarn login`
- Publish your package `yarn publish --access public`
```text
[1/4] Bumping version...
info Current version: 1.0.0
question New version: -> type your new version or empty to keep current version
```
- Go to [npm package](https://www.npmjs.com/package/@bcms-demo/new-project), you will see your package 
- Now you can install your package `npm i @bcms-demo/new-project` in your app
